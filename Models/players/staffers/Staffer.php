<?php
/**
 * Created by PhpStorm.
 * User: William Estrela
 * Date: 30/11/2017
 * Time: 17:25
 */

namespace Models\players\staffers;
//wubba lubba dub dub

use Models\Pessoa;

class Staffer extends Pessoa
{
    public $dataEntrada;
    public $idDiscord;

    /**
     * Staffer constructor.
     * @param $dataEntrada
     * @param $idDiscord
     */
    public function __construct($dataEntrada, $idDiscord, $nome, $nascimento, $numero)
    {
        parent::__construct($nome,$nascimento,$numero);
        $this->dataEntrada = $dataEntrada;
        $this->idDiscord = $idDiscord;
    }

    /**
     * @return mixed
     */
    public function getDataEntrada()
    {
        return $this->dataEntrada;
    }

    /**
     * @param mixed $dataEntrada
     */
    public function setDataEntrada($dataEntrada)
    {
        $this->dataEntrada = $dataEntrada;
    }

    /**
     * @return mixed
     */
    public function getIdDiscord()
    {
        return $this->idDiscord;
    }

    /**
     * @param mixed $idDiscord
     */
    public function setIdDiscord($idDiscord)
    {
        $this->idDiscord = $idDiscord;
    }


}