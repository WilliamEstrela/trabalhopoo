<?php
/**
 * Created by PhpStorm.
 * User: William Estrela
 * Date: 30/11/2017
 * Time: 17:24
 */

namespace Models\players;


use Models\Pessoa;

class Players extends Pessoa
{
    public $nick;

    /**
     * Players constructor.
     * @param $nick
     * @param $nome
     * @param $nascimento
     * @param $numero
     */
    public function __construct($nick, $nome, $nascimento, $numero)
    {
        parent::__construct($nome,$nascimento,$numero);
        $this->nick = $nick;
    }

    /**
     * @return mixed
     */
    public function getNick()
    {
        return $this->nick;
    }

    /**
     * @param mixed $nick
     */
    public function setNick($nick)
    {
        $this->nick = $nick;
    }

    public static function getClassName():string {
        return "Players";
    }
    public static function getIdAttribute():string{
        return "numero";
    }

}