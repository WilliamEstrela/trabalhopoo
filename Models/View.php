<?php
/**
 * Created by PhpStorm.
 * User: William Estrela
 * Date: 12/12/2017
 * Time: 21:18
 */

namespace Models;


class View
{



    public static function printFlashbags($model){
        foreach ($model->label->all() as $type=>$messages){
            foreach ($messages as $message) {
                echo '<div class="alert alert-' . $type . '">' . $message . '</div>';
            }
        }
    }
}