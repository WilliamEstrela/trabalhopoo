<?php
/**
 * Created by IntelliJ IDEA.
 * User: Alessandro
 * Date: 23/11/2017
 * Time: 11:33
 */

namespace Models\Persistence;
//wubba lubba dub dub
use Models\Erros\IDnotFound;
use Models\PersistenceInterface;
use phpDocumentor\Reflection\Types\Array_;

abstract class Arquivo implements PersistenceInterface
{
    public static function load(int $id): PersistenceInterface
    {
        $contents=file_get_contents(static::getClassName().".txt");
        $strings= explode(PHP_EOL,$contents);
        $attribute=static::getIdAttribute();

        foreach ($strings as $str){
            $obj=unserialize($str);
            if ($id==(int)$obj->$attribute){
                return $obj;
            }
        }
        throw new IDnotFound();
    }
    //["nome"=>"william", "idade" => "21"]
    public static function find($Params = array())
    {
        $data = array();
        $contents=file_get_contents(static::getClassName().".txt");
        $strings= explode(PHP_EOL,$contents);
        foreach ($strings as $str){
            $obj=unserialize($str);
            $cont = 0;
            foreach ($Params as $key => $value) {
                if ($obj->$key == $value) {
                    $cont++;
                }
            }
            if(count($Params) == $cont){
                $data[] = $obj;
            }
        }
        return $data;
        throw new IDnotFound();
    }

    public function delete()
    {
        $contents=file_get_contents($this->getClassName().".txt");
        $strings= explode(PHP_EOL,$contents);
        if ( $key=array_search( serialize($this), $strings)){
            if ($key === FALSE){
                throw new IDnotFound($this->getClassName()." Não encontrado");
            }
            unset($strings[$key]);
        }
        file_put_contents($this->getClassName() . ".txt", implode(
            PHP_EOL, $strings));
    }

    public function save()
    {
        $contents=file_get_contents($this->getClassName().".txt");
        $strings=[];
        if(!($contents==FALSE)){
            $strings= explode(PHP_EOL,$contents);
            $attribute=$this->getIdAttribute();
            foreach ($strings as &$str) {
                $obj = unserialize($str);
                if ($this->$attribute == $obj->$attribute) {
                    $str = serialize($this);
                    file_put_contents($this->getClassName() . ".txt", implode(
                        PHP_EOL, $strings));
                    return true;
                }
            }
        }
        $strings[]=serialize($this);
        file_put_contents($this->getClassName() . ".txt", implode(
            PHP_EOL, $strings));
    }
}