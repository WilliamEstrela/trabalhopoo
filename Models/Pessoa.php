<?php
/**
 * Created by PhpStorm.
 * User: William Estrela
 * Date: 30/11/2017
 * Time: 17:17
 */

namespace Models;
//wubba lubba dub dub

use Models\Persistence\Arquivo;

class Pessoa extends Arquivo
{
    public $nome;
    public $nascimento;
    public $numero;
    public $label;

    /**
     * Pessoa constructor.
     * @param $nome
     * @param $id
     * @param $nascimento
     * @param $numero
     */
    public function __construct($nome, $nascimento, $numero)
    {
        $this->nome = $nome;
        $this->nascimento = $nascimento;
        $this->numero = $numero;
    }

    public function label($label)
    {
        $this->label = $label;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getNascimento()
    {
        return $this->nascimento;
    }

    /**
     * @param mixed $nascimento
     */
    public function setNascimento($nascimento)
    {
        $this->nascimento = $nascimento;
    }

    /**
     * @return mixed
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * @param mixed $numero
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;
    }
    public static function getClassName():string {
        return "AHHHHHHHHHHdelicia";
    }
    public static function getIdAttribute():string{
        return "id";
    }

}