<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Cover Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="cover.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>-->
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]
</head>

<body>

<!--<div class="site-wrapper">-->

<!--    <div class="site-wrapper-inner">-->

<!--        <div class="cover-container">-->

<!--            <div class="masthead clearfix">-->
<!--                <div class="inner">-->
<!--                    <h3 class="masthead-brand">William ...</h3>-->
<!--                    <nav>-->
<!--                        <ul class="nav masthead-nav">-->
<!--                            <li class="active"><a href="#">Home</a></li>-->
<!--                            <li><a href="#">Features</a></li>-->
<!--                            <li><a href="#">Contact</a></li>-->
<!--                        </ul>-->
<!--                    </nav>-->
<!--                </div>-->
<!--            </div>-->

<!--            <div class="inner cover">-->
<!--                <h1 class="cover-heading">Cover your page.</h1>-->
<!--                <p class="lead">Cover is a one-page template for building simple and beautiful home pages. Download, edit the text, and add your own fullscreen background photo to make it your own.</p>-->
<!--                <p class="lead">-->
<!--                    <a href="#" class="btn btn-lg btn-default">Learn more</a>-->
<!--                </p>-->
<!--            </div>-->

<!--            <div class="mastfoot">-->
<!--                <div class="inner">-->
<!--                    <p>Cover template for <a href="http://getbootstrap.com">Bootstrap</a>, by <a href="https://twitter.com/mdo">@mdo</a>.</p>-->
<!--                </div>-->
<!--            </div>-->

<!--        </div>-->

<!--    </div>-->

<!--</div>-->

<!--<!-- Bootstrap core JavaScript-->
<!--================================================== -->
<!--<!-- Placed at the end of the document so the pages load faster -->
<!--wubba lubba dub dub-->
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>-->
<!--wubba lubba dub dub-->
<!--<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>-->
<!--<script src="../../dist/js/bootstrap.min.js"></script>-->
<!--wubba lubba dub dub-->
<!--<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<!--<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>-->
<!--</body>-->
<!--</html>-->
<!--wubba lubba dub dub-->

<?php
/**
 * Created by IntelliJ IDEA.
 * User: Alessandro
 * Date: 31/10/2017
 * Time: 10:08
 */

require_once __DIR__.'/../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing;
use Symfony\Component\Routing\Route;
use Symfony\Component\HttpKernel;
use Framework\Framework;

$request = Request::createFromGlobals();

$routes= new Routing\RouteCollection();//inicia o objeto para eu criar rotas

$routes->add('pessoaCreate',new Route('/pessoa/create',
    ['_controller'=>'Controllers\AddPessoa::formAction']));

$routes->add('playerCreate',new Route('/player/create',
    ['_controller'=>'Controllers\AddPlayer::formAction']));

$routes->add('playerview',new Route('/player/view',
    ['_controller'=>'Controllers\AddPlayer::findAction']));
$routes->add('playerviewD',new Route('/player/view/{player}',
    ['_controller'=>'Controllers\AddPlayer::detailAction']));
//
//$routes->add('servidorCreate',new Route('/player/create',
//    ['_controller'=>'Controllers\AddPessoa::addPessoaAction']));
//
//
//
//$routes->add('servidorCreate',new Route('/players/view',
//    ['_controller'=>'Controllers\ServidorController::createAction']));

$context = new Routing\RequestContext();
$matcher = new Routing\Matcher\UrlMatcher($routes,$context);

$controllerResolver= new HttpKernel\Controller\ControllerResolver();
$argumentResolver = new HttpKernel\Controller\ArgumentResolver();

$framework= new  Framework($matcher,$controllerResolver,$argumentResolver);
$response= $framework->handle($request);

$response->send();
?>
</body>
</html>
