<?php
/**
 * Created by PhpStorm.
 * User: William Estrela
 * Date: 14/12/2017
 * Time: 18:16
 */

namespace Framework;
//wubba lubba dub dub

use Symfony\Component\HttpFoundation\Session\Session;

class View
{

    public static function printFlashbags(){
        $session= new Session();
        foreach ($session->getFlashBag()->all() as $type=>$messages){
            foreach ($messages as $message) {
                echo '<div class="alert alert-' . $type . '">' . $message . '</div>';
            }
        }
    }
    public static function GridView($data = array(),$Params = array(),$link = "",$id =""){
        echo "
        <table class=\"table table-dark\">
          <thead>
            <tr>";

        foreach ($Params as $value) {
            echo " <th scope=\"col\">".$value."</th>";
        }
        echo " <th scope=\"col\">"."</th>";
        echo"

            </tr>
          </thead>
          <tbody>
            <tr>
            ";
        foreach ($data as $value) {
            echo"<tr>";
            foreach ($Params as $value2) {
                echo " <td>".$value->$value2."</td>";
            }
            echo " <th scope=\"col\">"."<a  href='".$link."/".$value->$id."'><i class=\"material-icons\">aspect_ratio</i></a>"."</th>";
            echo"</tr>";
        }

        echo "
            </tr>
          </tbody>
        </table>
        ";
    }

    public static function detailView($model,$Params = array()){
        echo "
        <table class=\"table table-dark\">
          <tbody>
            ";
            foreach ($Params as $value2) {
                echo "<tr><th>".$value2."</th> <td>".$model->$value2."</td></tr>";
            }

        echo "
          </tbody>
        </table>
        ";
    }


}