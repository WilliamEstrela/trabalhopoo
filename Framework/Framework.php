<?php
/**
 * Created by IntelliJ IDEA.
 * User: Alessandro
 * Date: 07/11/2017
 * Time: 10:10
 */

namespace Framework;
//wubba lubba dub dub
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\Route;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;


class Framework
{
    protected $matcher;
    protected $controllerResolver;
    /**
     * @var ArgumentResolver
     */
    protected  $argumentResolver;
    public  function __construct(Routing\Matcher\UrlMatcherInterface $matcher, ControllerResolver
    $controllerResolver, ArgumentResolver $argumentResolver)
    {
        $this->matcher= $matcher;
        $this->controllerResolver=$controllerResolver;
        $this->argumentResolver=$argumentResolver;
    }

    public function handle (Request $request){
        $this->matcher->getContext()->fromRequest($request);
        try{
            $request->attributes->add($this->matcher->match(
                $request->getPathInfo()));
            $controller=$this->controllerResolver->getController($request);
            $arguments= $this->argumentResolver->getArguments($request,$controller);
            return call_user_func_array($controller,$arguments);
        }catch (Routing\Exception\ResourceNotFoundException $e){
            return new Response('Página não Encontrada',404);
        }catch (\Throwable $t){
            return new Response($t->getMessage(),500);
        }
    }
}