<?php
/**
 * Created by PhpStorm.
 * User: William Estrela
 * Date: 30/11/2017
 * Time: 18:27
 */

namespace Controllers;
//wubba lubba dub dub
use Models\Form;
use Models\Pessoa;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

class AddPessoa
{
    private $model;

    private $session;

    function formAction(Request $request){
        $this->session= new Session();
        if ( $request->getMethod()=="POST"){
            try{
                $this->model= new Pessoa($request->request->get("nome"),$request->request->get("nascimento"),$request->request->get("numero"));
                $this->model->label("Nome");
                $this->model->save();
                $this->session->getFlashBag()->add('success',"Pessoa Cadastrada com Sucesso");
                echo "OKKKK";
                return 0;
            }catch (\Throwable $t){

            }
        }

        ob_start();
        $form= Form::Begin("/trabalhopoo/index.php/pessoa/create", "Adicionar Pessoa!");
        $form->input("text",$this->model,'nome');
        $form->input("text",$this->model,'nascimento');
        $form->input("text",$this->model,'numero');
        $form->end("Enviar");
        return new Response(ob_get_clean());

    }

}