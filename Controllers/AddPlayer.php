<?php
/**
 * Created by PhpStorm.
 * User: William Estrela
 * Date: 30/11/2017
 * Time: 19:12
 */

namespace Controllers;

use Framework\View;
use Models\Form;
use Models\Pessoa;
use Models\players\Players;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
//wubba lubba dub dub
class AddPlayer
{
    private $model;

    private $session;


    function findAction(Request $request)
    {
       $data = Players::find(["nome"=>"MITO", "nick"=>"testudo"]);
       View::printFlashbags();
       View::GridView($data,["nome","nick","nascimento"],"/trabalhopoo/index.php/player/view","numero");
    }

    function detailAction(Request $request,$player)
    {
      $model = Players::load($player);
      View::detailView($model,["nome","nick","nascimento","numero"]);
    }

    function formAction(Request $request){
        $this->session= new Session();
        if ( $request->getMethod()=="POST"){
            try{
                $this->model = new Players($request->request->get("nick"),$request->request->get("nome"),$request->request->get("nascimento"),$request->request->get("numero"));
                $this->model->save();
                $this->session->getFlashBag()->add('success',"Player cadastrado com sucesso");
                return new RedirectResponse('/trabalhopoo/index.php/player/view');
            }catch (\Throwable $t){

            }
        }

        ob_start();
        $form= Form::Begin("/trabalhopoo/index.php/player/create", "Adicionar player!");
        $form->input("text",$this->model,'nome');
        $form->input("text",$this->model,'nick');
        $form->input("text",$this->model,'nascimento');
        $form->input("text",$this->model,'numero');

        $form->end("Enviar");
        return new Response(ob_get_clean());

    }
}