<?php
/**
 * Created by PhpStorm.
 * User: William Estrela
 * Date: 15/12/2017
 * Time: 16:07
 */

namespace Tests\Models;


use Models\players\Players;
use PHPUnit\Framework\TestCase;

class PlayersTest extends TestCase
{


    /**
     * @dataProvider providerTestValidPlayers
     */
    public function testValidPlayers($nick,$nome,$nascimento,$numero){

        $player = new Players($nick,$nome,$nascimento,$numero);

        $this->assertEquals($nick,$player->getNick());
        $this->assertEquals($nome,$player->getNome());
        $this->assertEquals($nascimento,$player->getNascimento());
        $this->assertEquals($numero,$player->getNumero());

    }

    public function  providerTestValidPlayers(){

        return [
            ['WilliamEstrela','William Estrela Louzeiro','03/03/1997','62981312141'],
            ['FaAlpha','Fernando Augusto','01/01/2012','63981312588'],
        ];
    }
}