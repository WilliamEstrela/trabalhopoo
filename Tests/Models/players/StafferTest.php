<?php
/**
 * Created by PhpStorm.
 * User: William Estrela
 * Date: 15/12/2017
 * Time: 16:08
 */

namespace Tests\Models;


use Models\players\staffers\Staffer;
use PHPUnit\Framework\TestCase;

class StafferTest extends TestCase
{


    /**
     * @dataProvider providerTestValidStaffer
     */
    public function testValidStaffer($dataEntrada,$idDiscord,$nome,$nascimento,$numero){

        $staffer = new Staffer($dataEntrada,$idDiscord,$nome,$nascimento,$numero);

        $this->assertEquals($dataEntrada,$staffer->getDataEntrada());
        $this->assertEquals($idDiscord,$staffer->getIdDiscord());
        $this->assertEquals($nome,$staffer->getNome());
        $this->assertEquals($nascimento,$staffer->getNascimento());
        $this->assertEquals($numero,$staffer->getNumero());
    }

    public function  providerTestValidStaffer(){

        return [
            ['15/12/2017','WilliamEstrela#1029','William Estrela Louzeiro','03/03/1997','62981312141'],
            ['15/12/2017','FernandoAugusto#1029','Fernando Augusto','03/03/1998','63984312191'],

        ];
    }

}